export default class Lace {
	constructor() {
		this.s = 0
	}
	
	setup(sk,w,h) {
		sk.createCanvas(w,h);
		sk.background(255);
		sk.noFill();
		sk.blendMode(sk.MULTIPLY);
	}

	draw(sk) {
		sk.translate(sk.width>>1, sk.height>>1)
		this.s += 0.7;
		sk.stroke(sk.noise(this.s / 200) * 100 + 125, sk.noise(this.s / 200, 200) * 100 + 125, sk.noise(this.s / 200, 400) * 100 + 125, 50);
		sk.beginShape();
		for (let i = 0; i < 500; i++) {
			const v = sk.createVector(sk.noise(this.s / 200) * 700 - 700, 0);
			v.rotate(sk.TWO_PI / 500 * i);
			v.setMag(v.mag() - sk.noise(v.x / 300, v.y / 300, this.s / 2000) * 400)
			sk.vertex(v.x, v.y);
		}
		sk.endShape(sk.CLOSE);
	}
	
	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}
}
