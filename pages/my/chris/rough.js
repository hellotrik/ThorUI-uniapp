export default class Rough {
	constructor() {
		this.pts = []
		this.onPressed = false
		this.showInstruction = false
	}

	setup(sk, w, h) {
		sk.createCanvas(w, h, sk.P2D);
		sk.smooth();
		sk.frameRate(30);
		sk.colorMode(sk.RGB);
		sk.rectMode(sk.CENTER);
		this.showInstruction = true;
		sk.background(0);
	}

	draw(sk) {
		if (this.showInstruction) {
			sk.background(0);
			sk.fill(255);
			sk.textAlign(sk.CENTER, sk.CENTER);
			sk.textFont("Calibri", 24, true)
			sk.textLeading(36);
			sk.text("Drag and draw." + "\n" +
				"Press 'c' to clear the stage." + "\n", sk.width >> 1, sk.height >> 1);
		}

		if (this.onPressed) {
			for (let i = 0; i < 10; i++) {
				let newP = new Particle(sk, sk.mouseX, sk.mouseY, i + this.pts.length, i + this.pts.length);
				this.pts.push(newP);
			}
		}

		for (let i = 0; i < this.pts.length; i++) {
			let p = this.pts[i];
			p.update(sk);
			p.display(sk);
		}

		for (let i = this.pts.length - 1; i > -1; i--) {
			let p = this.pts[i];
			if (p.dead) {
				this.pts.splice(i, 1);
			}
		}
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {
		if (sk.key == 'c') {
			for (let i = this.pts.length - 1; i > -1; i--) {
				this.pts.splice(i, 1);
			}
			sk.background(255);
		}
	}
	mp(sk) {

		this.onPressed = true;
		if (this.showInstruction) {
			sk.background(255);
			this.showInstruction = false;
		}
	}
	mr(sk) {

		this.onPressed = false;
	}
	mv(sk) {}
	mw(sk, e) {}
}

class Particle {
	constructor(sk, x, y, xOffset, yOffset) {
		this.passedLife = 0
		this.dead = false
		this.alpha = 0
		this.weight = 0

		this.loc = sk.createVector(x, y);
		let randDegrees = sk.random(360);
		this.vel = sk.createVector(sk.cos(sk.radians(randDegrees)), sk.sin(sk.radians(randDegrees)));
		this.vel.mult(sk.random(5));
		this.acc = sk.createVector(0, 0);
		this.lifeSpan = sk.int(sk.random(30, 90));
		this.decay = sk.random(0.75, 0.9);
		this.c = sk.color(0, 0, sk.random(255));
		this.weightRange = sk.random(3, 50);
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	update(sk) {
		if (this.passedLife >= this.lifeSpan) {
			this.dead = true;
		} else {
			this.passedLife++;
		}

		this.alpha = sk.float(this.lifeSpan - this.passedLife) / this.lifeSpan * 70 + 20;
		this.weight = sk.float(this.lifeSpan - this.passedLife) / this.lifeSpan * this.weightRange;
		this.acc.set(0, 0);
		let rn = (sk.noise((this.loc.x + sk.frameCount + this.xOffset) * 0.01, (this.loc.y + sk.frameCount + this
				.yOffset) * 0.01) - 0.5) * 4 *
			sk.PI;
		let mag = sk.noise((this.loc.y + sk.frameCount) * 0.01, (this.loc.x + sk.frameCount) * 0.01);
		let dir = sk.createVector(sk.cos(rn), sk.sin(rn));
		this.acc.add(dir);
		this.acc.mult(mag);
		let randDegrees = sk.random(360);
		let randV = sk.createVector(sk.cos(sk.radians(randDegrees)), sk.sin(sk.radians(randDegrees)));
		randV.mult(0.5);
		this.acc.add(randV);
		this.vel.add(this.acc);
		this.vel.mult(this.decay);
		this.vel.limit(3);
		this.loc.add(this.vel);
	}

	display(sk) {
		sk.strokeWeight(this.weight + 1.5);
		sk.stroke(0, this.alpha);
		sk.point(this.loc.x, this.loc.y);
		sk.strokeWeight(this.weight);
		sk.stroke(this.c);
		sk.point(this.loc.x, this.loc.y);
	}
}
