export default class Art {
	constructor() {
		this.nmobiles = 4000;
		this.mobiles = [];
		this.ns = 1;
		this.a = []
		this.amax = 5
		this.bw = true
		this.w;
		this.h;
	}
	reset(sk) {
		this.ns = sk.random(.01, .1);
		sk.noiseDetail(sk.int(sk.random(1, 5)));
		this.amax = sk.random(5);
		for (let i = 0; i < 4; i++)
			this.a[i] = sk.random(1, this.amax)
		this.a[4] = 50;
		for (var i = 0; i < this.nmobiles; i++) {
			this.mobiles[i] = new Mobile(i, sk, this.a[0], this.a[1], this.a[2]);
		}
	}
	setup(sk, w, h) {
		this.w = w;
		this.h = h;
		sk.createCanvas(w, h);
		sk.background(0);
		sk.noFill();
		sk.colorMode(sk.HSB, 360, 255, 255, 255);
		sk.strokeWeight(.1);
		this.reset(sk);
	}

	draw(sk) {
		//noiseSeed(millis()*.00004);
		for (let i = 0; i < this.nmobiles; i++) {
			this.mobiles[i].run(sk, this.a[1], this.a[2], this.a[3], this.a[4]);
		}
	}
	wr(sk) {}
	kr(sk) {
		//if (key=="s" || key=="S")saveCanvas("svimg" + day() + "_" + month() + "_" + hour() + "_" + minute() + "_" + second() + ".jpg");
		if (sk.key == "s" || sk.key == "S") sk.saveCanvas("POSTHELIOS_NOISE3_" + sk.day() + "_" + sk.month() + "_" +
			sk.hour() + "_" +
			sk.minute() + "_" + sk.second() + ".png");
		console.log(sk.key + sk.keyCode)
		if (sk.keyCode == 32) {
			this.reset(sk);
		}
		if (sk.key == "r" || sk.key == "R") this.setup(sk, this.w, this.h);
		if (sk.key == "b" || sk.key == "B") this.bw = !this.bw;
	}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}
}

class Mobile {
	constructor(idx, sk, a1, a2, a3) {
		this.index = idx;
		this.velocity = sk.createVector(200, 200, 200);
		this.acceleration = sk.createVector(200, 200, 200);
		this.position0 = sk.createVector(sk.random(0, sk.width), sk.random(0, sk.height), sk.random(0, sk.sin(sk
			.height)));
		this.position = this.position0.copy();
		this.trans = sk.random(50, 100);
		//  this.hu=(noise(a1*sin(PI*this.position.x/width), a1*cos(PI*this.position.y/height))*720)%360;//random(360);
		this.hu = (sk.noise(a1 * sk.cos(sk.PI * this.position.x / sk.width), a1 * sk.sin(sk.PI * this.position.y /
			sk
			.height)) * 720) % sk.random(360);
		this.sat = sk.noise(a2 * sk.sin(sk.PI * this.position.x / sk.width), a2 * sk.sin(sk.PI * this.position.y /
			sk.height)) * 255;
		this.bri = sk.noise(a3 * sk.cos(sk.PI * this.position.x / sk.width), a3 * sk.cos(sk.PI * this.position.y /
			sk.height)) * 255;
	}

	run(sk, a2, a3, a4, a5) {
		this.update(sk, a2, a3, a4, a5);
		this.display(sk);
	}

	// Method to update position
	update(sk, a2, a3, a4, a5) {
		this.velocity = sk.createVector(1 - 2 * sk.noise(a4 + a2 * sk.sin(sk.TAU * this.position.x / sk.width),
				a4 + a2 * sk.sin(sk.TAU * this.position.y / sk.height)),
			1 - 2 * sk.noise(a2 + a3 * sk.cos(sk.TAU * this.position.x / sk.width),
				a4 + a3 * sk.cos(sk.TAU * this.position.y / sk.height)));

		this.velocity.mult(a5);
		//100*fbm(this.position);
		this.velocity.rotate(sk.sin(100) * sk.noise(a4 + a3 * sk.sin(sk.TAU * this.position.x / sk.width)));
		this.position0 = this.position.copy();
		this.position.add(this.velocity);
	};

	// Method to display
	display(sk) {
		if (this.bw) sk.stroke(255, this.trans);
		else sk.stroke((sk.frameCount * 1.8) % 360, (sk.millis() % 360), (sk.frameCount) % 360, this.trans % 255);
		//  if(bw)stroke(0,this.trans); else stroke(this.hu,this.sat,this.bri,this.trans);

		sk.line(this.position0.x, this.position0.y, this.position.x, this.position.y);


		if (this.position.x > sk.width || this.position.x < 0 || this.position.y > sk.height || this.position.y <
			0) {
			this.position0 = sk.createVector(sk.random(0, sk.width), sk.random(0, sk.height), sk.random(0, sk
				.height * sk
				.width));
			this.position = this.position0.copy();
		}
	};
}
