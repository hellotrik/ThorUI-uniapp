export default class Heart {
	constructor() {
		this.R = 10;
		this.delta = 0;
		this.d = [0, 0, 0]
		this.p = 1;
	}
	setup(sk, w, h) {
		sk.createCanvas(w, h);

		if (sk.width > sk.height) {
			this.R = sk.height / 400 * 10;
		} else {
			this.R = sk.width / 400 * 10;
		}

		this.d[0] = 10;
		this.d[1] = this.d[0] + this.p;
		this.d[2] = this.d[1] + this.p

		sk.background(100);
		sk.noFill();
		sk.strokeWeight(1);
		sk.strokeJoin(sk.ROUND);
		sk.angleMode(sk.DEGREES);
	}

	draw(sk) {
		sk.background(100, 10);
		for (let i = 0; i < 3; i++) {
			sk.stroke(i == 2 ? 0 : 255, i == 0 ? 0 : 255, i == 1 ? 0 : 255, 80);
			this.drawHeart(sk, this.d[i]);
		}
		this.delta += 0.001;
	}
	drawHeart(sk, c) {
		sk.push();
		sk.translate(sk.width >> 1, sk.height >> 1);
		sk.beginShape();
		for (let theta = 0; theta < 360; theta++) {
			let x = this.R * (16 * sk.sin(theta * this.delta * c) * sk.sin(theta) * sk.sin(theta));
			let y = (-1) * this.R * (13 * sk.cos(theta) - 5 * sk.cos(2 * theta) -
				2 * sk.cos(3 * theta) - sk.cos(4 * theta));
			sk.vertex(x, y);
		}
		sk.endShape(sk.CLOSE);
		sk.pop();
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}

}
