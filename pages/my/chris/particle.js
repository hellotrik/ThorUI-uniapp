export default class {
	constructor() {
		this.particles = [];
	}

	setup(sk, w, h) {
		sk.createCanvas(w, h);
		//createCanvas(800, 800);
		sk.pixelDensity(1);
		sk.colorMode(sk.HSB, 255);
		//blendMode(ADD);
	}
	draw(sk) {
		sk.clear();
		sk.background("black");
		let newParticles = [];
		for (let i = 0; i < this.particles.length; i++) {
			this.particles[i].update();
			this.particles[i].wallBound(sk);
			this.particles[i].display(sk);
			if (this.particles[i].radius > 0) {
				newParticles.push(this.particles[i]);
			}
		}
		this.particles = newParticles;
	}
	wr(sk) {
		sk.resizeCanvas(sk.windowWidth, sk.windowHeight);
	}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {
		var x = sk.mouseX;
		var y = sk.mouseY;
		var vx = (sk.winMouseX - sk.pwinMouseX) * 0.2;
		var vy = (sk.winMouseY - sk.pwinMouseY) * 0.2;
		if ((x > 50 && x < sk.width - 50) && (y > 50 && y < sk.height - 50)) {
			this.particles.push(new Particle(sk, x, y, vx, vy));
		}
	}
	mw(sk, e) {}

}

class Particle {
	constructor(sk, x, y, vx, vy) {
		this.position = sk.createVector(x, y);
		this.velocity = sk.createVector(vx, vy);
		this.friction = 0.005;
		this.h = 255 * sk.abs(sk.cos((sk.frameCount / 600) * sk.PI));
		this.fcolor = sk.color(this.h, 255, 255, 100);
		this.ecolor = sk.color(this.h, 255, 255, 200);
		this.radius = sk.random(5, 50);
		this.shape = sk.floor(sk.random(3, 33));
	}

	update() {
		this.velocity = this.velocity.mult(1 - this.friction);
		this.position = this.position.add(this.velocity);
		this.radius -= 0.1;
	}
	wallThrough(sk) {
		if (this.position.x >= sk.width) {
			this.position.x = 0;
		}
		if (this.position.x <= 0) {
			this.position.x = sk.width;
		}
		if (this.position.y >= sk.height) {
			this.position.y = 0;
		}
		if (this.position.y <= 0) {
			this.position.y = sk.height;
		}
	}
	wallBound(sk) {
		if ((this.position.x >= sk.width - this.radius) || (this.position.x <= this.radius)) {
			this.velocity.x = -this.velocity.x;
		} else if ((this.position.y >= sk.height - this.radius) || (this.position.y <= this.radius)) {
			this.velocity.y = -this.velocity.y;
		} else {
			return;
		}
	}

	display(sk) {
		sk.push();
		sk.translate(this.position.x, this.position.y);
		//noStroke();
		sk.stroke(this.ecolor);
		sk.strokeWeight(1);
		sk.fill(this.fcolor);
		sk.ellipse(0, 0, 2 * this.radius, 2 * this.radius, sk.floor(this.shape));
		//if (this.shape == 0) {
		//  ellipse(0, 0, 2*this.radius, 2*this.radius, 32);
		//} else {
		//  rect(0, 0, 2*this.radius, 2*this.radius);
		//}
		//sphere(this.radius);
		sk.pop();
	}

}
