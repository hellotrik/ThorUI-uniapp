export default class Seraph {
	constructor() {
		//float a = 0.008;  // three wings
		//float m = -0.496; // three wings
		this.a = 0.01; // five wings
		this.m = Math.cos(4 * Math.PI / 5) + 0.008; // five wings
		this.b = 0.05;
		this.cx = 1; // value of current x
		this.cy = 1; // value of current y
		this.d = 1; // diameter of an ellipse
		this.s = 0.05; // change scale of the map
		this.t = 0; // keep track of time
		this.showLine = Math.random()<0.5
	}
	setup(sk, w, h) {
		sk.createCanvas(w, h);
		sk.background(20, 10, 10);
		sk.noStroke();
		//frameRate(120);
	}

	draw(sk) {
		this.t++;
		for (let i = 0; i < 30; i++) {
			// update x and y
			let newX = this.cy + this.a * (1 - this.b * sk.sq(this.cy)) * this.cy + this.m * this.cx + 2 * (1 - this
				.m) * sk.sq(this.cx) / (1 + sk.sq(this.cx));
			let newY = -this.cx + this.m * newX + 2 * (1 - this.m) * sk.sq(newX) / (1 + sk.sq(newX));

			// calculate the distance between the new point and the center
			let distance = sk.dist(sk.int(sk.width / 2 + newX / this.s), sk.int(sk.height / 2 + newY / this.s), (sk
				.width / 2), sk.height / 2);

			// color accoriding to the distance
			let n = sk.noise(this.t / 30);

			// draw ellipse
			if (distance >= 0 && distance < 100 * n) sk.fill(202, 114, 65); //fill(255, 0, 0);
			else if (distance >= 100 * n && distance < 200 * n) sk.fill(182, 134, 85); //fill(255, 255, 0);  
			else if (distance >= 200 * n && distance < 350 * n) sk.fill(162, 164, 105); //fill(0, 255, 0); 
			else if (distance >= 350 * n && distance < 500 * n) sk.fill(142, 204, 125); //fill(0, 0, 255); 
			else sk.fill(122, 254, 205); //fill(255);
			sk.ellipse(sk.int(sk.width / 2 + newX / this.s), sk.int(sk.height / 2 + newY / this.s), this.d, this.d);

			////drawing in line is also interesting!
			if (this.showLine) {
				if (distance >= 0 && distance < 320 * n) sk.stroke(202, 114, 65, 5); //fill(255, 0, 0);
				else if (distance >= 320 * n && distance < 420 * n) sk.stroke(182, 134, 85,
					5); //fill(255, 255, 0);  
				else if (distance >= 420 * n && distance < 520 * n) sk.stroke(162, 164, 105, 5); //fill(0, 255, 0); 
				else if (distance >= 520 * n && distance < 620 * n) sk.stroke(142, 204, 125, 5); //fill(0, 0, 255); 
				else sk.stroke(122, 254, 205, 5); //fill(255);
				sk.line(sk.int(sk.width / 2 + newX / this.s), sk.int(sk.height / 2 + newY / this.s), sk.int(sk
					.width / 2 + this.cx / this.s), sk.int(sk.height / 2 + this.cy / this.s));
			}

			// save the point to variables
			this.cx = newX;
			this.cy = newY;
		}
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}

}
