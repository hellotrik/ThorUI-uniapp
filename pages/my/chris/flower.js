export default class Flower {
	constructor() {
		this.xy = []
		this.off = []
		this.inc = []
		this.angle = []
		this.count = 0;

		this.palette = ["#FF7700", "#DB4513", "#6C9AAA", "##225B6F", "#003041"];
		this.palette2 = [];
		this.currentColor = [this.palette[0]]
	}
	setup(sk, w, h) {
		sk.createCanvas(w, h);
		for (let i = 0; i < 3; i++) {
			this.xy[i] = sk.width * (i == 0 ? .4 : sk.random(.4))
			this.xy[i + 3] = sk.height * (i == 0 ? .4 : sk.random(.4))
			this.off[i] = 0.0
			this.inc[i] = sk.random(0.0075)
		}
		sk.noFill();
		this.currentColor = sk.color(this.palette[Math.floor(sk.random(this.palette.length))]);
		sk.alpha(20);
	}
	draw(sk) {
		if (this.count < 1200) {
			sk.translate(sk.width / 2, sk.height / 2);

			if (this.count % 100 == 0) {
				this.currentColour = sk.color(this.palette[Math.floor(sk.random(this.palette.length))]);
			}

			sk.stroke(sk.red(this.currentColour), sk.green(this.currentColour), sk.blue(this.currentColour), 20);
			sk.bezier(0, 0, this.xy[2] * sk.cos(sk.radians(this.angle[2])), this.xy[5] * sk.sin(sk.radians(this
					.angle[2])), this.xy[1] * sk.cos(
					sk.radians(this.angle[1])),
				this.xy[4] * sk.sin(sk.radians(this.angle[1])), this.xy[0] * sk.cos(sk.radians(this.angle[0])),
				this.xy[3] * sk.sin(sk.radians(
					this.angle[0])));

			this.angle[0] = sk.noise(this.off[0]) * sk.width;
			this.off[0] += this.inc[0];
			this.angle[1] = sk.noise(this.off[1]) * sk.width;
			this.off[1] += this.inc[1];
			this.angle[2] = sk.noise(this.off[2]) * sk.width;
			this.off[2] += this.inc[2];
			sk.translate(-sk.width / 2, 0);

			this.count += 1;
		}
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}

}
