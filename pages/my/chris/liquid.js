export default class Liquid {
	constructor() {}
	setup(sk, w, h) {
		sk.createCanvas(w, h);
		sk.background(51, 51, 51);
		sk.smooth();
		sk.textFont("STXingkai")
		sk.textSize(80);
		sk.textAlign(sk.CENTER, sk.CENTER);
	}
	draw(sk) {
		sk.stroke(51, 71, 51, 5);
		sk.fill(51, 51, 51, 10);
		sk.text("神 莹 内 敛 \n Tai Ran Zi Ruo", sk.width >> 1, sk.height >> 1);
		for (let i = +10; i < sk.height - 10; i += 0.5) {
			var Iy = sk.map(sk.mouseX, sk.width, 0, 1, 10);
			var Ix = sk.map(sk.mouseX, 0, sk.width, 1, 10);
			sk.stroke(255, 0, 255, 15); // yellow
			sk.fill(255, 255, 0, 150);
			sk.rect(sk.noise(i * 0.002, -sk.frameCount * 0.002) * sk.width, i, Iy, Iy); // ble

			sk.stroke(0, 255, 0, 15);
			sk.fill(0, 0, 255, 150);
			sk.rect(sk.noise(2 * i * 0.002, sk.frameCount * 0.001) * sk.width, i, Ix, Ix); // red
		}
		sk.noStroke();
		sk.fill(51, 51, 51, 255);
		sk.rect(0, 0, sk.width, 10);
		sk.rect(0, sk.height - 10, sk.height, sk.height);
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {
		sk.noStroke();
		sk.fill(51, 51, 51, 255);
		sk.rect(0, 0, sk.width, sk.height);
	}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}
}
