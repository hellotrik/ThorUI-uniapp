export default class Boat {
	constructor(x) {
		this.x = x || 40
		this.m = 0
		this.y = 0
	}
	setup(sk,w,h) {
		sk.createCanvas(w,h)
	}
	chuan(sk, x) {
		sk.fill(237, 253, 255);
		sk.noStroke();
		sk.triangle(165 + x, 215, 195 + x, 165, 195 + x, 215);
		sk.triangle(195 + x, 220, 195 + x, 160, 225 + x, 205);

		sk.fill(60, 162, 176);
		sk.noStroke();
		sk.ellipse(210 + x, 225, 88, 2);
		sk.ellipse(210 + x, 230, 60, 2);
		sk.ellipse(195 + x, 193, 3, 70);
		sk.ellipse(180 + x, 215, 25, 2);

		//ren
		sk.fill(126, 94, 62);
		sk.noStroke();
		sk.ellipse(230 + x, 213, 8, 8);

		sk.fill(60, 162, 176);
		sk.noStroke();
		sk.rect(225 + x, 213, 10, 12, 3);

		sk.fill(247, 194, 112);
		sk.noStroke();
		sk.ellipse(230 + x, 207, 8, 5);
		sk.ellipse(230 + x, 209, 20, 3);
	}
	draw(sk) {
		sk.background(255);
		sk.fill(219, 251, 226);
		sk.noStroke();
		sk.ellipse(265, 178, 280, 280);

		//taiyang
		sk.fill(246, 236, 173);
		sk.noStroke();
		sk.ellipse(200, 100, 60, 60);

		sk.fill(255, 221, 138);
		sk.noStroke();
		sk.ellipse(200, 100, 48, 48);

		//yunduo1
		sk.fill(255);
		sk.noStroke();
		sk.ellipse(150, 120, 28, 28);

		sk.fill(255);
		sk.noStroke();
		sk.ellipse(170, 118, 36, 36);

		sk.fill(255);
		sk.noStroke();
		sk.ellipse(185, 120, 18, 26);

		//yunduo2
		sk.fill(255);
		sk.noStroke();
		sk.ellipse(235, 85, 20, 20);

		sk.fill(255);
		sk.noStroke();
		sk.ellipse(250, 85, 28, 28);

		sk.fill(255);
		sk.noStroke();
		sk.ellipse(262, 85, 16, 18);

		//shan
		sk.fill(160, 244, 220);
		sk.noStroke();
		sk.ellipse(255, 200, 70, 160);

		sk.fill(131, 235, 203);
		sk.noStroke();
		sk.ellipse(228, 200, 70, 120);

		sk.fill(104, 221, 185);
		sk.noStroke();
		sk.ellipse(156, 200, 125, 120);

		sk.fill(158, 245, 220);
		sk.noStroke();
		sk.ellipse(356, 200, 86, 228);

		sk.fill(104, 221, 185);
		sk.noStroke();
		sk.ellipse(296, 200, 108, 150);

		sk.fill(104, 221, 185);
		sk.noStroke();
		sk.ellipse(358, 190, 30, 30);

		sk.fill(104, 221, 185);
		sk.noStroke();
		sk.ellipse(404, 200, 80, 88);

		sk.fill(255);
		sk.noStroke();
		sk.rect(0, 195, 532, 134);

		//he
		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(60, 195, 410, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(100, 205, 324, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(90, 215, 296, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(165, 225, 185, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(140, 235, 245, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(185, 245, 150, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(100, 255, 265, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(200, 265, 110, 10, 5);

		sk.fill(192, 249, 210);
		sk.noStroke();
		sk.rect(395, 228, 45, 10, 5);

		sk.fill(255);
		sk.noStroke();
		sk.rect(140, 205, 30, 10, 5);

		sk.fill(255);
		sk.noStroke();
		sk.rect(280, 225, 35, 10, 5);

		sk.fill(255);
		sk.noStroke();
		sk.ellipse(315, 250, 10, 10);
		sk.translate(this.y, 0);
		this.chuan(sk, this.x);
		this.y = 80 * sk.sin(this.m);
		this.m += 0.005;
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}
}
