export default class Wave {
	constructor() {
		this.r = 10;
		this.distort = 5;
	}

	setup(sk, w, h) {
		sk.createCanvas(w, h, sk.P3D)
		sk.background(0, 0, 20);
		//cam=new PeasyCam(this,300);
		sk.frameRate(100);
	}


	draw(sk) {
		sk.background(0);
		sk.translate(0, 0, -this.r + 200);
		sk.noFill();
		sk.stroke(sk.random(255), sk.random(255), 255, 255 - this.r);
		let s = sk.random(10) > 5 ? true : false;


		if (this.r < 250) {
			this.r += 1;
			this.conc(sk, s);
		} else {
			this.distort += 1;
			//background(0,0,20);
			this.conc(sk, s);
			//r=10;
		}
		if (sk.frameCount % 200 == 0) {
			sk.background(0, 0, 20);
			this.r = 10;
		}
	}
	conc(sk, q) {
		let max = sk.random(sk.PI * 2);
		for (let a = sk.random(sk.PI * 2); a < max + sk.PI / 2; a += 0.01) {
			let r1 = sk.map(sk.noise(a * this.r), 0, 1, this.r - this.distort, this.r + this.distort);
			let x = (sk.width >> 1) + sk.cos(a) * r1;
			let y = (sk.height >> 2) + sk.sin(a) * this.r;
			sk.ellipse(x, y, 2, 1);
		}
	}

	wr(sk) {}
	kr(sk) {}
	kp(sk) {}
	mp(sk) {}
	mr(sk) {}
	mv(sk) {}
	mw(sk, e) {}


}
