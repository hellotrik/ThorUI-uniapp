export default class Nebula {
	constructor(arg) {
		this.num = 1000; //the number of point(s).
		this.mts = Math.PI / 24; //max theta speed.
		this.r = 100; //radius of the circle
		this.rdtr = 5; //range of the rdt
		this.rdu = 1; //radius of circle
		//**********
		this.v = []
		this.mv = true;
		this.mo = true;
		this.c = []; //color of each point.
		this.theta = []; //original angle of each point.
		this.mtheta = []; //translate angle to math value.
		this.dtheta = []; //speed of theta.
		this.easing = [];
		this.rdt = []; //make a shuffle of radius.
	}
	setup(sk, w,h) {
		sk.colorMode(sk.RGB, 255, 255, 255);
		sk.createCanvas(w,h);
		for (let i = 0; i < this.num - 1; i++) {
			this.c[i] = sk.color(sk.random(100, 200), sk.random(100, 200), sk.random(100, 200));
			this.v[i] = sk.createVector(sk.random(sk.width), sk.random(sk.height));
			this.theta[i] = sk.round(sk.random(360));
			this.dtheta[i] = sk.random(this.mts);
			this.mtheta[i] = this.theta[i] / 180 * sk.PI;
			this.rdt[i] = sk.round(sk.random(-this.rdtr, this.rdtr));
			this.easing[i] = sk.random(0.02, 0.3);
		}
		sk.frameRate(60);
	}

	vo(sk, i, a = 1) {
		this.v[i].lerp(sk.mouseX + sk.cos(this.mtheta[i]) * (this.rdt[i] + a * this.r), sk.mouseY + sk.sin(
				this.mtheta[i]) * (this.rdt[i] + a * this.r), 0,
			this.easing[i]);
		sk.fill(this.c[i]);
		sk.ellipse(this.v[i].x, this.v[i].y, this.rdu, this.rdu);
	}

	draw(sk) {
		sk.fill(25, 25, 25, 25);
		sk.rect(0, 0, sk.width, sk.height);
		sk.applyMatrix();
		sk.noStroke();
		for (let i = 0; i < this.num - 1; i++) {
			if (this.mo)
				this.mtheta[i] += this.dtheta[i];
			this.vo(sk, i, this.mv ? 1 : 0)
		}
		sk.resetMatrix();
		sk.fill(0);
		sk.rect(0, 0, sk.width, 15);
		sk.fill(255);
		sk.textAlign(sk.LEFT, sk.TOP);
		sk.text("r = " + this.r, 0, 0);
		sk.text("fps = " + sk.round(sk.frameRate), 40, 0);
		if (this.mv) {
			sk.fill(255, 0, 0);
			sk.text("Running", 100, 0);
		} else {
			sk.text("Static", 100, 0);
		}
		if (this.mo) {
			sk.fill(255, 0, 0);
			sk.text("motion", 150, 0);
		} else {
			sk.fill(255);
			sk.text("stop", 150, 0);
		}
	}
	wr(sk) {}
	kr(sk) {}
	kp(sk) {
		if (sk.key == 's' || sk.key == 'S') {
			this.mo = !this.mo;
		}
	}
	mp(sk) {
		/*for(int i = 0;i<num-1;i++){
		 v[i] = new PVector(random(width),random(height));
		 theta[i] = round(random(360));
		 mtheta[i] = theta[i]/180*PI;
		 dtheta[i] = random(mts);
		 rdt[i] = round(random(-rdtr,rdtr)+r);
		 }*/
		this.mv = !this.mv;
	}
	mr(sk) {}
	mv(sk) {}
	mw(sk, evt) {
		let e = evt.delta;
		console.log(e)
		if (e < 0) this.r += 10;
		if (e > 0) this.r -= 10;
	}
}
